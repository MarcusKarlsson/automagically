from django.db import models
import core.td
import signals.models
from settings.models import getSettings
import time
import Queue
import subprocess
import os
import signal

PLUGIN_NAME = os.path.splitext(os.path.split(__file__)[1])[0] #This will be the filename without .py

debug = False

global currentSettings
currentSettings = {}

proc = None
workQueue = Queue.Queue()

def signalHandler(signal):
    global currentSettings
    if not currentSettings or signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        currentSettings = getSettings(PLUGIN_NAME)
        try:
            global proc
            if proc:
                if debug:
                    print 'Sending sigint to cec-client'
                proc.send_signal(signal.SIGINT)
                if debug:
                    print 'done sending'
        except:
            raise
            pass

        if debug:
            print 'hdmi-cec got configuration,changed signal'

    if signal.content.startswith('hdmi-cec,do:'):
        outcmd = signal.content[len('hdmi-cec,do:'):]
        if debug:
            print 'hdmi-cec send command:', repr(outcmd.strip())
        proc.stdin.write(outcmd.strip() + '\n')
        proc.stdin.flush()

    elif signal.content.startswith('hdmi-cec,rawenable:'):
        pass


def threadFunc():
    global currentSettings
    currentSettings = getSettings(PLUGIN_NAME)

    keepRunning = True
    while(keepRunning):
        if not currentSettings.get('Enabled', True):
            if debug:
                print 'hdmi-cec not enabled'
            time.sleep(60)
            continue

        try:
            cmd = '/home/pi/source/automagically/bin/./cec-client -m'
            if debug:
                print 'Executing the following command:', cmd

            global proc
            proc = subprocess.Popen(cmd, stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.STDOUT, shell = True)

            while(1):
                out = proc.stdout.readline()
                if out == '':
                    keepRunning = False
                    if debug:
                        print 'Received nothing breaking out'
                    break

                if debug:
                    print repr(out)

                try:
                    if out.startswith('NOTICE:') and currentSettings.get('Forward notice messages', False):
                        outmsg = out.split(None, 3)[3].replace(' ', '_')
                        signals.models.postToQueue('hdmi-cec:notice,' + outmsg, PLUGIN_NAME)
                    if out.startswith('DEBUG:') and currentSettings.get('Forward debug messages', False):
                        outmsg = out.split(None, 3)[3].replace(' ', '_')
                        signals.models.postToQueue('hdmi-cec:debug,' + outmsg, PLUGIN_NAME)
                    if out.startswith('TRAFFIC:') and currentSettings.get('Forward traffic messages', False):
                        outmsg = out.split(None, 3)[3].replace(' ', '_')
                        signals.models.postToQueue('hdmi-cec:traffic,' + outmsg, PLUGIN_NAME)
                except:
                    print 'Error when postToQueue'

        except:
            raise

        print 'hdmi-cec (cec-client) has exited, needs to be restarted'

        if proc:
            try:
                print signal
                proc.send_signal(signal.SIGINT)
            except:
                raise
            proc.wait()
            proc = None

        if keepRunning == False:
            break

        if debug:
            print 'Waiting 5 sec before trying to run cec-client again'
            time.sleep(10)
        else:
            print 'Waiting a minute before trying to run cec-client again'
            time.sleep(60)



    


def init():
    global init_done
    init_done = True

    settings = {'Forward debug messages': ('bool', False),
                'Forward notice messages':    ('bool',  True),
                'Forward traffic messages':    ('bool',  False),
                'Enabled': ('bool', True),
}

    return (PLUGIN_NAME, settings, signalHandler, threadFunc)

