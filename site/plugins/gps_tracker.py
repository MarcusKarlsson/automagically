#
#
# File:         gps_tracker.py
# Version:      0.4
#
# Description:  An Automagically plugin for the GPS tracket TK110, 
#               from http://www.ebay.com/itm/121438088073
#               Listens to a socket and handles the following type of messages:
#               Heartbeat: 
#               (027043469204BP00000027043469204HSO) leads to response (027043469204AP01HSO)
#               Isochronous feedback: 
#               (027043469204BR00150701A5612.8039N01533.5692E000.015362893.40000000000L00000000)
#
#               The protocol specification can be found here:
#               http://sourceforge.net/p/opengts/discussion/579835/thread/37a0491d/cf88/attachment/GPS%20tracker%20Communication__%20Protocol%20V1.51.pdf
#
# Usage:        Install in ~/source/automagically/site/plugins/, restart Automagically
#
# Provides:     gps_tracker,data;status:OK;IMEI:XX;latitude:XX.xx;longitude:XX.xx;distance:XX.xx;geofence:XX;speed:XX.x;date:XX;time:XX;google:XX; 
#               - IMEI, 12 digits
#               - latitude, Decimal Degrees
#               - longitude, Decimal Degrees
#               - distance, kilometers from geofence mid-point
#               - geofence  [INSIDE / OUTSIDE]
#               - speed, UNKNOWN format
#               - date, YYMMDD
#               - time, Time in UTC HHMMSS
#               - google, URL to position on google maps
#               eg.
#               gps_tracker,data;status:OK;IMEI:027043469204;latitude:59.327118;longitude:18.071359;distance:376.959732774;geofence:OUTSIDE;speed:000.5;date:150630;time:053425;google:http://maps.google.com/?ie=UTF8&hq=&ll=56.213345,15.588177&z=13;
#
#
#
#
#               gps_tracker,data;fence:XX;IMEI:XX;google:http://maps.google.com/?ie=UTF8&hq=&ll=56.213345,15.588177&z=13; 
#               - fence, LEAVING or ENTERING the geo fence
#               - IMEI, 12 digits
#               - google, URL to position on google maps
#
# Pattern to 
# match:        gps_tracker,data;status:$1;IMEI:$2;latitude:$3;longitude:$4;distance:$5;geofence:$6;speed:$7;date:$8;time:$9;
#               $1 - status
#               $2 - IMEI
#               $3 - latitude
#               $4 - longitude
#               $5 - distance
#               $6 - geofence
#               $7 - speed
#               $8 - date
#               $9 - time
#
# Settings:     The plugin has the following settings
#               Enabled = True/False; The plugin is enabled or disabled. Default: False
#               Socket = The socket that the plugin listens to. Default 10000
#
# Dependencies: The plugin depends on that there exists  Decide Type: GpsDevice
#               and is not compatible with the "bulc" ASutomagically
#               
# Usage:        Add geo fences by adding GpsDevices. Leave IMEI blank to make it valid for
#               all devices, or set IMEI to match what the client send to make it unique
#
# Installation: Install new ~/source/automagically/site/core/admin.py and ~/source/automagically/site/core/models.py
#               sudo service apache2 stop
#               sudo service automagically stop
#               sudo rm ~/source/automagically/site/core/admin.pyc
#               sudo rm ~/source/automagically/site/core/models.pyc
#               cd ~/source/automagically/site
#               sudo python manage.py schemamigration core --auto
#               sudo python manage.py migrate core
#               sudo service apache2 stop
#               sudo service automagically stop
#

# Imports
from django.db import models
from settings.models import getSettings
import signals.models
from core.models import GpsDevice
import Queue
import socket
import sys
import thread
import math

# Plugin configurations
PLUGIN_NAME = 'gps_tracker'
PLUGIN_ENABLED = 'Enabled'
PLUGIN_SOCKET = 'Socket'

PLUGIN_SIGNAL_SEND = "gps_tracker,data;status:"
PLUGIN_FENCE_SEND = "gps_tracker,data;fence:"
PLUGIN_TIMEOUT = 15

# Global variables
debug = False
workQueue = Queue.Queue()
fences = []

def getDeviceFences():
    global fences
    fences =[]
    for fence in GpsDevice.objects.all():
        fences.append([fence.name, fence.IMEI, fence.latitude, fence.longitude, fence.distance, "UNKNOWN"])
    return

def checkFences(IMEI, latitude_dd, longitude_dd):
    global fences
    
    myFences = []
    
    for fence in fences:
        if fence[1].strip() == '' or fence[1].strip() == IMEI:
            distance = 6373 * distance_on_unit_sphere(latitude_dd, longitude_dd, fence[2], fence[3])
            if distance > fence[4]:
                gps_fence_position = "OUTSIDE"
            else:
                gps_fence_position = "INSIDE"
                
            direction = "STABLE"
            if fence[5] != gps_fence_position and fence[4] != "UNKNOW":
                if  fence[5] == "INSIDE":
                    direction = "LEAVING"
                    
                elif  fence[5] == "OUTSIDE":
                    direction = "ENTERING"
            
            fence[5] = gps_fence_position
            
            myFences.append([fence[0], distance, gps_fence_position, direction])
            
    return myFences

def signalHandler(signal):
    if debug:
        print PLUGIN_NAME + ' received signal:' + signal.content.strip()
    if signal.content == 'terminate':
        workQueue.put('terminate')
    elif signal.content.strip() == PLUGIN_NAME + ',configuration,changed':
        workQueue.put('update')
    elif signal.content.startswith(PLUGIN_NAME + ',action:'): # Note: Change to your needs
        # Received an 'action' signal
        for i in signal.content.strip()[19:].split(';'):      # Note: 19 = len("gps_tracker,action:")
            if i != '':
                workQueue.put(i)

def distance_on_unit_sphere(lat1, long1, lat2, long2):
 
    # Convert latitude and longitude to 
    # spherical coordinates in radians.
    degrees_to_radians = math.pi/180.0
         
    # phi = 90 - latitude
    phi1 = (90.0 - lat1)*degrees_to_radians
    phi2 = (90.0 - lat2)*degrees_to_radians
         
    # theta = longitude
    theta1 = long1*degrees_to_radians
    theta2 = long2*degrees_to_radians
         
    # Compute spherical distance from spherical coordinates.
         
    # For two locations in spherical coordinates 
    # (1, theta, phi) and (1, theta, phi)
    # cosine( arc length ) = 
    #    sin phi sin phi' cos(theta-theta') + cos phi cos phi'
    # distance = rho * arc length
     
    cos = (math.sin(phi1)*math.sin(phi2)*math.cos(theta1 - theta2) + 
           math.cos(phi1)*math.cos(phi2))
    arc = math.acos( cos )
 
    # Remember to multiply arc by the radius of the earth 
    # in your favorite set of units to get length.
    return arc

def parse_tracker_data(msg):
    
    # Validate it's a correct head and tail
    if not isinstance(msg, str):
        return "ERROR 01"+'\r\n'
    if(len(msg) < 19):
        return "ERROR 02"+'\r\n'
    if msg[0] != '(' and msg[len(msg) - 1] != ')':
        return "ERROR 03"+'\r\n'
    
    identifier = msg[1:1+12]
    command = msg[13:13+4]
    
    if command == "BP00": # Heartbeet
        retval = '(' + identifier + 'AP01HSO)'+'\r\n'
        return retval
        
    elif command == "BR00": # Position
        # Date
        offset = 17
        offset_end = offset + 6
        date = msg[offset:offset_end]
        
        # Availability
        offset = offset_end
        offset_end = offset + 1
        availability = msg[offset:offset_end]

        # Latitude
        offset = offset_end
        offset_end = offset + 9
        latitude = msg[offset:offset_end]

        # Latitude indicator
        offset = offset_end
        offset_end = offset + 1
        latitude_ind = msg[offset:offset_end]

        # Longitude
        offset = offset_end
        offset_end = offset + 10
        longitude = msg[offset:offset_end]
        
        # Longitude Indicator
        offset = offset_end
        offset_end = offset + 1
        longitude_ind = msg[offset:offset_end]

        # Speed
        offset = offset_end
        offset_end = offset + 5
        speed = msg[offset:offset_end]
        
        # Time
        offset = offset_end
        offset_end = offset + 6
        time = msg[offset:offset_end]
        
        # Orientation
        offset = offset_end
        offset_end = offset + 6
        orientation = msg[offset:offset_end]

        # IOState
        offset = offset_end
        offset_end = offset + 8
        iostate = msg[offset:offset_end]

        # Milepost (L)
        offset = offset_end
        offset_end = offset + 1
        milepost = msg[offset:offset_end]

        # Mileage
        offset = offset_end
        offset_end = offset + 8
        mileage = msg[offset:offset_end]

        if availability == 'A':
            latitude_dd = round(float(latitude[0:2])  + float(latitude[2:2+7]) /60, 6)
            if latitude_ind != "N":
                latitude_dd = - latitude_dd
                
            longitude_dd = round(float(longitude[0:3]) + float(longitude[3:3+7])/60, 6)
            if longitude_ind != "E":
                longitude_dd = - longitude_dd
            
            maps_url = "http://maps.google.com/maps/?q=loc:" + str(latitude_dd) + "," + str(longitude_dd) + "&z=15"

            ret = PLUGIN_SIGNAL_SEND + "OK" + ";IMEI:"      + identifier + \
                                              ";latitude:"  + str(latitude_dd) + \
                                              ";longitude:" + str(longitude_dd) + \
                                              ";speed:"     + speed + \
                                              ";date:"      + date + \
                                              ";time:"      + time + \
                                              ";google:"    + maps_url + \
                                              ";"
            
            signals.models.postToQueue(ret, PLUGIN_NAME)

            # Check for fences [name, distance, gps_fence_position, direction]
            myFences = checkFences(identifier, latitude_dd, longitude_dd)
            for fence in myFences:
                if fence[3] != "STABLE":
                    signals.models.postToQueue(PLUGIN_FENCE_SEND + fence[0] +";IMEI:" + identifier + ";direction:" + fence[3] + ";google:" + maps_url + ";", PLUGIN_NAME)
                else:
                    signals.models.postToQueue(PLUGIN_FENCE_SEND + fence[0] +";IMEI:" + identifier + ";position:" + fence[2] + ";google:" + maps_url + ";", PLUGIN_NAME)
            
               
        elif availability == 'V':
            ret = PLUGIN_SIGNAL_SEND + "FAIL";
            signals.models.postToQueue(ret, PLUGIN_NAME)
        
        return "NO_RESPONSE"

def clientthread(conn, enabled):
    if debug:
        print PLUGIN_NAME + ' Setting timeout in client'
    conn.settimeout(PLUGIN_TIMEOUT)
    
    while enabled():
         
        # Receiving from client
        try:
            if debug:
                print PLUGIN_NAME + ' Waiting for data'
            data = conn.recv(1024)
            if debug:
                print PLUGIN_NAME + ' Received: ' + data.rstrip()

        except socket.timeout:
            if debug:
                print PLUGIN_NAME + ' Socket timeout, over again'
            if enabled():
                continue
            else:
                break

        if not data: 
            break

        reply = parse_tracker_data(data)
    
        if(reply != "NO_RESPONSE"):
            if debug:
                print PLUGIN_NAME + ' Sending: ' + reply.rstrip()
            conn.sendall(reply)
     
    # Came out of loop
    if debug:    
        print PLUGIN_NAME + ' Shutting down client socket'
    conn.close()
                
def threadFunc():
    currentSettings = {}
    keepRunning = True
    nextUpdate = 60 #seconds

    # Load plugin settings
    currentSettings = getSettings(PLUGIN_NAME)
    pluginEnabled = currentSettings[PLUGIN_ENABLED]
    pluginSocket = currentSettings[PLUGIN_SOCKET]
    
    # Read Geo Fences
    getDeviceFences()
    
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(PLUGIN_TIMEOUT)
    
    # Bind the socket to the port
    server_address = ('0.0.0.0', pluginSocket)
    if debug:
        print PLUGIN_NAME + ' Starting up on %s port %s' % server_address
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(server_address)
    sock.listen(5)
    
    # Main loop
    while(keepRunning):
        try:
            
            try:
                if pluginEnabled:
                    s = workQueue.get(False) # Don't wait, we need to poll since we hang on socket later
                else:
                    s = workQueue.get(True) # Wait, we don't want to loop without wait
                
            except Queue.Empty: # Normally when nothing to read (at False)
                # Nothing in the queue
                if pluginEnabled:
                
                    # Wait on the socket
                    try:
                        if debug:
                            print PLUGIN_NAME + ' Listening on socket in the loop'
                        connection, client_address = sock.accept()
                        if debug:
                            print PLUGIN_NAME + ' Start new thread'
                        thread.start_new_thread(clientthread ,(connection, lambda: pluginEnabled))

                    except socket.timeout:
                        continue
                        
                    # Back to main loop to check the queue
                    continue

            if s == 'terminate': # Time to close down
                if debug:
                    print PLUGIN_NAME + ' time to close down'
                workQueue.task_done()
                if pluginEnabled:
                    # Close the socket
                    sock.shutdown(socket.SHUT_RDWR)
                    sock.close()
                keepRunning = False
                pluginEnabled = False
                continue
                
            elif s == 'update': # Configuration has changed, update to new values

                if pluginEnabled:
                    # Close the socket
                    sock.shutdown(socket.SHUT_RDWR)
                    sock.close()
                
                if debug:
                    print PLUGIN_NAME + ' settings updated'
                
                currentSettings = getSettings(PLUGIN_NAME)
                pluginEnabled = currentSettings[PLUGIN_ENABLED]
                pluginSocket = currentSettings[PLUGIN_SOCKET]
                
                # Read Geo Fences
                getDeviceFences()

                if pluginEnabled:
                    # Create a TCP/IP socket
                    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    sock.settimeout(PLUGIN_TIMEOUT)
                    
                    # Bind the socket to the port
                    server_address = ('0.0.0.0', pluginSocket)
                    if debug:
                        print PLUGIN_NAME + ' Starting up on %s port %s' % server_address
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    sock.bind(server_address)
                    sock.listen(5)
                
                workQueue.task_done()
                continue
                      
            else: # A signal was received
                if debug:
                    print PLUGIN_NAME + ' got signal:' + s
                if pluginEnabled:
                    ##
                    ## CODE HERE TO ACT WHEN A SIGNAL IS RECEIVED
                    ##
                    if debug:
                        print "Acting on signal for plugin " + PLUGIN_NAME                        
                    
                workQueue.task_done()
                continue

        except Exception as e:
            print 'Error in ' + PLUGIN_NAME + ' threadfunction'
            print e.message, e.args
            if debug:
                raise

def init():
    settings = {PLUGIN_ENABLED: ('boolean', False),
                PLUGIN_SOCKET:  ('integer', 10000)}
    return (PLUGIN_NAME, settings, signalHandler, threadFunc)
