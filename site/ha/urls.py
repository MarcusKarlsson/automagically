from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^signals/', include('signals.urls')),
    url(r'^core/', include('core.urls')),
    url(r'^remote/', include('remote.urls')),
    url(r'', include('core.urls')),
)
