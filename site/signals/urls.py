from django.conf.urls import patterns, include, url

urlpatterns = patterns('signals.views',
    # Examples:
    # url(r'^$', 'btdt.views.home', name='home'),
    url(r'^$', 'send'),
    url(r'^index/$', 'index'),
    url(r'^create/$', 'create'),
    url(r'^create/tellstick/$', 'createtellstick'),
    url(r'^create/tellstick/(?P<rawcommand>\S+)$', 'createtellstick'),
    url(r'^create/(?P<inp>\S+)$', 'create'),
    url(r'^(?P<inp>\S+)$', 'send'),
)
