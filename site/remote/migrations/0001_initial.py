# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Theme'
        db.create_table('remote_theme', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal('remote', ['Theme'])

        # Adding model 'Remote'
        db.create_table('remote_remote', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('theme', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['remote.Theme'])),
            ('theme_config', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
        ))
        db.send_create_signal('remote', ['Remote'])

        # Adding model 'Page'
        db.create_table('remote_page', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('config', self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True)),
        ))
        db.send_create_signal('remote', ['Page'])

        # Adding model 'Widget'
        db.create_table('remote_widget', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('displayText', self.gf('django.db.models.fields.CharField')(default='', max_length=25, blank=True)),
            ('remote', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['remote.Remote'])),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['remote.Page'])),
            ('x', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('y', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('xSize', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('ySize', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
        ))
        db.send_create_signal('remote', ['Widget'])

        # Adding model 'SingleDevCmd'
        db.create_table('remote_singledevcmd', (
            ('widget_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['remote.Widget'], unique=True, primary_key=True)),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Device'])),
            ('command', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Command'])),
        ))
        db.send_create_signal('remote', ['SingleDevCmd'])

        # Adding model 'OnOffDev'
        db.create_table('remote_onoffdev', (
            ('widget_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['remote.Widget'], unique=True, primary_key=True)),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Device'])),
        ))
        db.send_create_signal('remote', ['OnOffDev'])

        # Adding model 'DimDev'
        db.create_table('remote_dimdev', (
            ('widget_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['remote.Widget'], unique=True, primary_key=True)),
            ('device', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.Device'])),
        ))
        db.send_create_signal('remote', ['DimDev'])

        # Adding model 'VariableValue'
        db.create_table('remote_variablevalue', (
            ('widget_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['remote.Widget'], unique=True, primary_key=True)),
            ('variable', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['core.GlobalVariable'])),
        ))
        db.send_create_signal('remote', ['VariableValue'])

    def backwards(self, orm):
        # Deleting model 'Theme'
        db.delete_table('remote_theme')

        # Deleting model 'Remote'
        db.delete_table('remote_remote')

        # Deleting model 'Page'
        db.delete_table('remote_page')

        # Deleting model 'Widget'
        db.delete_table('remote_widget')

        # Deleting model 'SingleDevCmd'
        db.delete_table('remote_singledevcmd')

        # Deleting model 'OnOffDev'
        db.delete_table('remote_onoffdev')

        # Deleting model 'DimDev'
        db.delete_table('remote_dimdev')

        # Deleting model 'VariableValue'
        db.delete_table('remote_variablevalue')

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'core.globalvariable': {
            'Meta': {'object_name': 'GlobalVariable'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastUpdated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'remote.dimdev': {
            'Meta': {'object_name': 'DimDev', '_ormbases': ['remote.Widget']},
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.onoffdev': {
            'Meta': {'object_name': 'OnOffDev', '_ormbases': ['remote.Widget']},
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.page': {
            'Meta': {'object_name': 'Page'},
            'config': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'remote.remote': {
            'Meta': {'object_name': 'Remote'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'theme': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Theme']"}),
            'theme_config': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'})
        },
        'remote.singledevcmd': {
            'Meta': {'object_name': 'SingleDevCmd', '_ormbases': ['remote.Widget']},
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'device': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.theme': {
            'Meta': {'object_name': 'Theme'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'remote.variablevalue': {
            'Meta': {'object_name': 'VariableValue', '_ormbases': ['remote.Widget']},
            'variable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.widget': {
            'Meta': {'object_name': 'Widget'},
            'displayText': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '25', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Page']"}),
            'remote': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Remote']"}),
            'x': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'xSize': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'y': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'ySize': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['remote']