# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Theme.maxX'
        db.add_column('remote_theme', 'maxX',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=5),
                      keep_default=False)

        # Adding field 'Theme.maxY'
        db.add_column('remote_theme', 'maxY',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=0),
                      keep_default=False)

    def backwards(self, orm):
        # Deleting field 'Theme.maxX'
        db.delete_column('remote_theme', 'maxX')

        # Deleting field 'Theme.maxY'
        db.delete_column('remote_theme', 'maxY')

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'core.globalvariable': {
            'Meta': {'object_name': 'GlobalVariable'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastUpdated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'remote.dimdev': {
            'Meta': {'object_name': 'DimDev', '_ormbases': ['remote.Widget']},
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.onoffdev': {
            'Meta': {'object_name': 'OnOffDev', '_ormbases': ['remote.Widget']},
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.page': {
            'Meta': {'object_name': 'Page'},
            'config': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'remote.remote': {
            'Meta': {'object_name': 'Remote'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'theme': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Theme']"}),
            'theme_config': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'})
        },
        'remote.singledevcmd': {
            'Meta': {'object_name': 'SingleDevCmd', '_ormbases': ['remote.Widget']},
            'cmd': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.theme': {
            'Meta': {'object_name': 'Theme'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maxX': ('django.db.models.fields.PositiveIntegerField', [], {'default': '5'}),
            'maxY': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'remote.variablevalue': {
            'Meta': {'object_name': 'VariableValue', '_ormbases': ['remote.Widget']},
            'var': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.widget': {
            'Meta': {'object_name': 'Widget'},
            'displayText': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '25', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Page']"}),
            'remote': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Remote']"}),
            'x': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'xSize': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'y': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'ySize': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['remote']