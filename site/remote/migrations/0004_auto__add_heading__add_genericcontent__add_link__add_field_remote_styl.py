# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Heading'
        db.create_table('remote_heading', (
            ('widget_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['remote.Widget'], unique=True, primary_key=True)),
            ('heading', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
            ('divider', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('remote', ['Heading'])

        # Adding model 'GenericContent'
        db.create_table('remote_genericcontent', (
            ('widget_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['remote.Widget'], unique=True, primary_key=True)),
            ('content', self.gf('django.db.models.fields.TextField')(default='')),
        ))
        db.send_create_signal('remote', ['GenericContent'])

        # Adding model 'Link'
        db.create_table('remote_link', (
            ('widget_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['remote.Widget'], unique=True, primary_key=True)),
            ('linktext', self.gf('django.db.models.fields.TextField')()),
            ('targetpage', self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['remote.Page'], null=True, blank=True)),
            ('url', self.gf('django.db.models.fields.TextField')(default='', blank=True)),
        ))
        db.send_create_signal('remote', ['Link'])

        # Adding field 'Remote.style'
        db.add_column('remote_remote', 'style',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

    def backwards(self, orm):
        # Deleting model 'Heading'
        db.delete_table('remote_heading')

        # Deleting model 'GenericContent'
        db.delete_table('remote_genericcontent')

        # Deleting model 'Link'
        db.delete_table('remote_link')

        # Deleting field 'Remote.style'
        db.delete_column('remote_remote', 'style')

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'core.globalvariable': {
            'Meta': {'object_name': 'GlobalVariable'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'unit': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10', 'blank': 'True'})
        },
        'remote.dimdev': {
            'Meta': {'object_name': 'DimDev', '_ormbases': ['remote.Widget']},
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.genericcontent': {
            'Meta': {'object_name': 'GenericContent', '_ormbases': ['remote.Widget']},
            'content': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.heading': {
            'Meta': {'object_name': 'Heading', '_ormbases': ['remote.Widget']},
            'divider': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'heading': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.link': {
            'Meta': {'object_name': 'Link', '_ormbases': ['remote.Widget']},
            'linktext': ('django.db.models.fields.TextField', [], {}),
            'targetpage': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': "orm['remote.Page']", 'null': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.onoffdev': {
            'Meta': {'object_name': 'OnOffDev', '_ormbases': ['remote.Widget']},
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.page': {
            'Meta': {'object_name': 'Page'},
            'config': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'remote.remote': {
            'Meta': {'object_name': 'Remote'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'style': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'theme': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Theme']"}),
            'theme_config': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'})
        },
        'remote.singledevcmd': {
            'Meta': {'object_name': 'SingleDevCmd', '_ormbases': ['remote.Widget']},
            'cmd': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.theme': {
            'Meta': {'object_name': 'Theme'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maxX': ('django.db.models.fields.PositiveIntegerField', [], {'default': '5'}),
            'maxY': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'remote.variablevalue': {
            'Meta': {'object_name': 'VariableValue', '_ormbases': ['remote.Widget']},
            'var': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.widget': {
            'Meta': {'object_name': 'Widget'},
            'displayText': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '25', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Page']"}),
            'remote': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Remote']"}),
            'x': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'xSize': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'y': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'ySize': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['remote']