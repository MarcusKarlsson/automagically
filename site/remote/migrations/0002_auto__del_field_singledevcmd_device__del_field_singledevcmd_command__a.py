# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'SingleDevCmd.device'
        db.delete_column('remote_singledevcmd', 'device_id')

        # Deleting field 'SingleDevCmd.command'
        db.delete_column('remote_singledevcmd', 'command_id')

        # Adding field 'SingleDevCmd.dev'
        db.add_column('remote_singledevcmd', 'dev',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['core.Device']),
                      keep_default=False)

        # Adding field 'SingleDevCmd.cmd'
        db.add_column('remote_singledevcmd', 'cmd',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['core.Command']),
                      keep_default=False)

        # Deleting field 'DimDev.device'
        db.delete_column('remote_dimdev', 'device_id')

        # Adding field 'DimDev.dev'
        db.add_column('remote_dimdev', 'dev',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['core.Device']),
                      keep_default=False)

        # Deleting field 'OnOffDev.device'
        db.delete_column('remote_onoffdev', 'device_id')

        # Adding field 'OnOffDev.dev'
        db.add_column('remote_onoffdev', 'dev',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['core.Device']),
                      keep_default=False)

        # Deleting field 'VariableValue.variable'
        db.delete_column('remote_variablevalue', 'variable_id')

        # Adding field 'VariableValue.var'
        db.add_column('remote_variablevalue', 'var',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['core.GlobalVariable']),
                      keep_default=False)

    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'SingleDevCmd.device'
        raise RuntimeError("Cannot reverse this migration. 'SingleDevCmd.device' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'SingleDevCmd.command'
        raise RuntimeError("Cannot reverse this migration. 'SingleDevCmd.command' and its values cannot be restored.")
        # Deleting field 'SingleDevCmd.dev'
        db.delete_column('remote_singledevcmd', 'dev_id')

        # Deleting field 'SingleDevCmd.cmd'
        db.delete_column('remote_singledevcmd', 'cmd_id')


        # User chose to not deal with backwards NULL issues for 'DimDev.device'
        raise RuntimeError("Cannot reverse this migration. 'DimDev.device' and its values cannot be restored.")
        # Deleting field 'DimDev.dev'
        db.delete_column('remote_dimdev', 'dev_id')


        # User chose to not deal with backwards NULL issues for 'OnOffDev.device'
        raise RuntimeError("Cannot reverse this migration. 'OnOffDev.device' and its values cannot be restored.")
        # Deleting field 'OnOffDev.dev'
        db.delete_column('remote_onoffdev', 'dev_id')


        # User chose to not deal with backwards NULL issues for 'VariableValue.variable'
        raise RuntimeError("Cannot reverse this migration. 'VariableValue.variable' and its values cannot be restored.")
        # Deleting field 'VariableValue.var'
        db.delete_column('remote_variablevalue', 'var_id')

    models = {
        'core.command': {
            'Meta': {'object_name': 'Command'},
            'argInt': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'cmd': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'id': ('django.db.models.fields.PositiveIntegerField', [], {'primary_key': 'True'})
        },
        'core.device': {
            'Meta': {'object_name': 'Device'},
            'activate': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'dim': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '40'}),
            'onOff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'order': ('django.db.models.fields.PositiveIntegerField', [], {'default': '100'})
        },
        'core.globalvariable': {
            'Meta': {'object_name': 'GlobalVariable'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastUpdated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'remote.dimdev': {
            'Meta': {'object_name': 'DimDev', '_ormbases': ['remote.Widget']},
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.onoffdev': {
            'Meta': {'object_name': 'OnOffDev', '_ormbases': ['remote.Widget']},
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.page': {
            'Meta': {'object_name': 'Page'},
            'config': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        'remote.remote': {
            'Meta': {'object_name': 'Remote'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'theme': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Theme']"}),
            'theme_config': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'})
        },
        'remote.singledevcmd': {
            'Meta': {'object_name': 'SingleDevCmd', '_ormbases': ['remote.Widget']},
            'cmd': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Command']"}),
            'dev': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Device']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.theme': {
            'Meta': {'object_name': 'Theme'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '15'})
        },
        'remote.variablevalue': {
            'Meta': {'object_name': 'VariableValue', '_ormbases': ['remote.Widget']},
            'var': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.GlobalVariable']"}),
            'widget_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['remote.Widget']", 'unique': 'True', 'primary_key': 'True'})
        },
        'remote.widget': {
            'Meta': {'object_name': 'Widget'},
            'displayText': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '25', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Page']"}),
            'remote': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['remote.Remote']"}),
            'x': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'xSize': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'y': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'ySize': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['remote']