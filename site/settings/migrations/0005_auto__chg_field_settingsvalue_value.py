# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'SettingsValue.value'
        db.alter_column('settings_settingsvalue', 'value', self.gf('django.db.models.fields.CharField')(max_length=255))
    def backwards(self, orm):

        # Changing field 'SettingsValue.value'
        db.alter_column('settings_settingsvalue', 'value', self.gf('django.db.models.fields.CharField')(max_length=20))
    models = {
        'settings.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'present': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'settings.settingsvalue': {
            'Meta': {'object_name': 'SettingsValue'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['settings.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'validationFunction': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['settings']