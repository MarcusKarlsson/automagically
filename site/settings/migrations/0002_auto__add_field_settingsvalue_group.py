# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SettingsValue.group'
        db.add_column('settings_settingsvalue', 'group',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['settings.Group']),
                      keep_default=False)

        # Removing M2M table for field subSettings on 'Group'
        db.delete_table('settings_group_subSettings')

    def backwards(self, orm):
        # Deleting field 'SettingsValue.group'
        db.delete_column('settings_settingsvalue', 'group_id')

        # Adding M2M table for field subSettings on 'Group'
        db.create_table('settings_group_subSettings', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('group', models.ForeignKey(orm['settings.group'], null=False)),
            ('settingsvalue', models.ForeignKey(orm['settings.settingsvalue'], null=False))
        ))
        db.create_unique('settings_group_subSettings', ['group_id', 'settingsvalue_id'])

    models = {
        'settings.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '25'})
        },
        'settings.settingsvalue': {
            'Meta': {'object_name': 'SettingsValue'},
            'dataType': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['settings.Group']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['settings']