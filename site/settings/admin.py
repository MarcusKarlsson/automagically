from settings.models import Group, SettingsValue
from django.forms import ModelForm
from django.contrib import admin


class SettingsValueInline(admin.TabularInline):
    model = SettingsValue
    fk_name = 'group'

    readonly_fields = ('description', 'dataType')

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class GroupAdmin(admin.ModelAdmin):
    readonly_fields = ('name',)
    inlines = [
        SettingsValueInline,
    ]
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False



class SettingsValueAdmin(admin.ModelAdmin):
    list_display = ('group', 'description', 'value', 'dataType')
    list_editable = ('value',)
    list_filter = ('group',)

    ordering = ('group', 'description')
 

    fields = ('group', 'description', 'value', 'dataType')
    readonly_fields = ('group', 'description', 'dataType')

    actions = None


    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
        




#admin.site.register(Group, GroupAdmin)
admin.site.register(SettingsValue, SettingsValueAdmin)
